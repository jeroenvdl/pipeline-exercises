# pipeline-training

Working with git and letting pipelines do the heavy lifting is a common practice nowadays, so here are some exercises to get to know the techniques. In this session we'll get to grips with building, testing and generally doing "stuff" with pipelines attached to git repositories. Most of the info that you'll need can be found in this [Reference](https://docs.gitlab.com/ee/ci/yaml/README.html "GitLab CI/CD Pipeline Configuration Reference"), I advice you to use the right side navigation pane "On this page:" and "Ctrl+f" on that page. Where applicable I'll try linking to the specific parts of that document. If all goes to plan at the end of this you'll have some more experience with a full blown multi stage build, test, deploy sytem with approval gates like the ones used by most big projects. ([Mailman](https://gitlab.com/mailman/mailman/pipelines), [GnuTLS](https://gitlab.com/gnutls/gnutls/pipelines), [Inkscape](https://gitlab.com/inkscape/inkscape/pipelines), [F-Droid](https://gitlab.com/fdroid/fdroidclient/pipelines))

## Contents

1. How does it work
2. Fork this repo
3. Bash commands
4. Artifacts
5. Multi stage
6. Approvals

## How does it work

Basically it all started with [git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks), they provide a way to run predefined bits of code at certain git state changes, and can be configured locally to do things like linting before pushing code to the remote or on the remote to build standardized packages from your source code.

### Git hooks

A list of all the git hooks and where they are executed.

#### local

- applypatch-msg
- pre-applypatch
- post-applypatch
- pre-commit
- prepare-commit-msg
- commit-msg
- post-commit
- pre-rebase
- post-checkout
- post-merge
- pre-receive

#### remote

- update; **This is where owners prevent you from pushing to master ;-)**
- post-receive; **This is where most pipelines hook into**
- post-update
- pre-auto-gc
- post-rewrite
- pre-push

## Fork this repo

Make sure you are logged in to gitlab.com, if you don't have an account create one [here](https://gitlab.com/users/sign_in#register-pane).

Visit this url: [Fork me](https://gitlab.com/cloud-people/pipeline-exercises/-/forks/new) and choose a destination to fork it to, usualy your own gitlab username.
Clone the resulting repo locally and have a look at some basic git hook examples and how these work with:

```bash
ls -lah .git/hooks
```

> For bonus points try to edit some of the scripts and get it to do something useless and fun.

## Bash commands

At this point you should have a fork of the original repo cloned on your machine, let's start with creating a basic pipeline that executes a few bash commands to get a feel for the process.


### `.gitlab-ci.yml`

 > Create this file in the root of the repo

This is a super minimal gitlab pipelinethat just prints the date.

```yaml
image: registry.access.redhat.com/ubi8/ubi-minimal

pipeline:
  script:
    - date
```

### what's in this yml

As you can see .gitlab-ci.yml uses the very popular [yaml](https://learnxinyminutes.com/docs/yaml/) [syntax](https://yaml.org/) let's see what is defined in this file:

- **image:** Here you specify which [docker image](https://hub.docker.com/_/ubuntu) to use for your pipeline run.
- **pipeline:** This can be anything as it names your job.
- **script:** This is a mandatory and takes in a `list` of `single line` bash commands.
- **- date** An item in the list of script items, runs the date command.

Add and commit this new file, push it to gitlab.
```bash
git add .gitlab-ci.yml 
git commit -m "Added gitlab-ci.yml"
git push
```
Take a look at the CI/CD > Pipelines tab. Too see your job ouput click on the build number which should also be tagged latest.
Click on the Jobs tab and click on the Job ID.

#### Congrats, you've just created a gitlab pipeline ;-)

> Now let's put that thing to use!

Pipelines can do the stuff you're to lazy to do yourself like:

- compiling source code
- run linting
- run tests
- packaging software ready for consumption
- security testing containers
- deploy the application to a cloud provider or container platform.
- load testing your application to see if it is ready for real use

### Compile source code

For this example we'll use some oldschool C source code, add the below code to hello.c in this repo.

```C
#include<stdio.h>            //Pre-processor directive
int main()                   //main function declaration
{
  printf("Hello World!\n");  //to output the string on a display
  return 0;                  //terminating function
}
```

Compile this locally.

```Bash
gcc hello.c -o hello
```
Run the result.
```txt
# ./hello
Hello World!
#
```

Now this is pretty simple, the project you're thinking about applying this is probably way more complicated but the principal stays the same.
Taking some source and if needed pull in dependencies then make it ready to be run by the end user.
Now let's have a look at letting our fresh new pipeline build this for us by adding some more commands to `.gitlab-ci.yml`

```yaml
image: registry.access.redhat.com/ubi8/ubi-minimal

pipeline:
  script:
    - gcc hello.c -o hello
    - ./hello
```
Commit this updated `.gitlab-ci.yml` and add the source code. Have a look at the output in the gitlab web interface.
```bash
git add .gitlab-ci.yml hello.c
git commit -m "Updatet gitlab-ci.yml to run hello.c"
git push
```

---

Well that was embarrassing, or was it?

Working with pipelines you often need to think about how you can get the runner to the point where it can do it's job like compiling some C code.
Adding features to your pipelines usually is a repeat of this process; add code, check result, adjust till it works.
Now fix your pipeline and celebrate your victory!

```yaml
image: registry.access.redhat.com/ubi8/ubi-minimal

pipeline:
  script:
    - microdnf install -y gcc
    - gcc hello.c -o hello
    - ./hello
```
Commit this updated `.gitlab-ci.yml` and have a look at the output in the gitlab web interface.
```bash
git add .gitlab-ci.yml
git commit -m "Updatet gitlab-ci.yml to run hello.c"
git push
```

Commit this updated `.gitlab-ci.yml` and have a look at the output in the gitlab web interface.
```bash
git add .gitlab-ci.yml hello.c
git commit -m "Updatet gitlab-ci.yml to run hello.c"
git push
```

Installing and managing some dependencies for one pipeline isn't a big deal but if you manage 10's, 100's or even more pipelines you might to centralize some of that heavy lifting and use a different base image that includes some build tools. For example:
- https://gitlab.com/aapjeisbaas/builder
- https://gitlab.com/conclusionxforce/container/tools-container
  - IMAGE: registry.gitlab.com/conclusionxforce/container/tools-container:latest
- https://gitlab.com/jeroenvdl/tools-container
  - IMAGE: registry.gitlab.com/jeroenvdl/tools-container:latest

## Artifacts

After building this awesome hello world binary we might want to show this off to the world by saving it as an artifact of our pipeline.
Have a go at exporting your `hello` binary [RTFM](https://docs.gitlab.com/ee/ci/yaml/#artifacts) ;-)

Artifacts can also be very useful to store build log files to be able to better debug failed builds.

I can think of some scenario's where it might be useful to have other artifacts depending on the type of build or the state of the build.
In git there's a common practice of tagging versions, try to export your hello binary as hello-release-${version} for tagged master builds.

```txt
# hint: add a git tag to your current HEAD
git tag 0.1.0
git push origin --tags
```

